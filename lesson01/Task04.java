package lesson01;
/*
create an array with 11 elements;
initialize array;
write method for iterative inversion of array: in-place, out-place.
show that it really works
 */

import java.util.Arrays;

public class Task04 {
    public static void main(String[] args) {
        int[] array = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 666};
        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(invertOutplace(array)));
        invertInplace(array);
        System.out.println(Arrays.toString(array));
    }

    private static void invertInplace(int[] array) {
        int middleOfRange = array.length / 2;
        for (int i = 0; i < middleOfRange; i++) {
            swapValueByIndices(array, i, array.length - 1 - i);
        }
    }

    private static int[] invertOutplace(int[] array) {
        int[] resultArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            resultArray[i] = array[array.length - 1 - i];
        }
        return resultArray;
    }
    private static void swapValueByIndices(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }

}