package lesson01;
/*
implement in-place
method, that swaps nearby values in array from 0 to [array.length-1] using for-loop
print initial array before and after
*/

import java.util.Arrays;

public class Task06 {
    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,0};
        System.out.println(Arrays.toString(array));
        swapAllValues(array);
        System.out.println(Arrays.toString(array));
    }

    private static void swapAllValues(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            swapValuesByIndices(array, i, i + 1);
        }
    }

    private static void swapValuesByIndices(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}