package lesson01;
/*
implement in-place method, that copies part of one array to another
void arrayCopy(int[] sourceArray, int startPosition, int[] destinationArray,int destinationPosition, int countOfElementsToCopy) {
print initial array "before" and "after"
*/

import java.util.Arrays;

public class Task08 {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        System.out.println("before:\t" + Arrays.toString(array));
        customArrayCopy(array, 1, array, 5, 5);
        System.out.println("after:\t" + Arrays.toString(array));
    }

    private static void customArrayCopy(int[] srcArray, int startPos,int[] destArray, int destPos, int countOfElements){
        int dest=destPos;
        for (int i = startPos; i < startPos+countOfElements; i++) {
            destArray[dest]=srcArray[i];dest++;
        }
    }
}