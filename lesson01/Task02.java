package lesson01;

import java.util.Arrays;

/*
1.Create 2d int array with random(10...20) count of elements
2.fill an array with random(0...100) numbers;
3.print array to consoleL:
a)as argument System.out.println();
b)using Arrays.toString()
c)using Arrays.deepToString()
d)using self-made method for 2d-array output
 */

public class Task02 {
    public static void main(String[] args) {
        int[][] sample2dArray = new int[(int) (Math.random() * 20)][(int) (Math.random() * 20)];
        fill2dArrayWithRandomNumbers(sample2dArray);
        System.out.println("native toString:\t" + sample2dArray);
        System.out.println("Arrays.toString:\t" + Arrays.toString(sample2dArray));
        System.out.println("Arrays.deepToString:\t" + Arrays.deepToString(sample2dArray));
        System.out.print("custom output:  \t");
        printArray(sample2dArray);
    }

    private static void printArray(int[][] array) {
        System.out.println();
        for (int[] subArray : array) {
            for (int num : subArray) {
                System.out.print(num + "\t");
            }
            System.out.println();
        }
    }

    private static void fill2dArrayWithRandomNumbers(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = (int) (Math.random() * 100);
            }
        }
    }
}
