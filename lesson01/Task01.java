package lesson01;

import java.util.Arrays;

/*
1.Create int array with 10 elements
2.fill an array with random numbers;
3.print array to consoleL:
a)as argument System.out.println();
b)using Arrays.toString()
c)using self-made method for array output

 */
public class Task01 {
    public static void main(String[] args) {
        int[] sampleArray = new int[10];
        fillArrayWithRandomNumbers(sampleArray);
        System.out.println("native toString:\t" + sampleArray);
        System.out.println("Arrays.toString:\t"+Arrays.toString(sampleArray));
        System.out.print("custom output:  \t");
        printArray(sampleArray);
    }

    private static void printArray(int[] array) {
        for(int num:array) {
            System.out.print(num+" ");
        }
    }

    private static void fillArrayWithRandomNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i]= (int) (Math.random()*100);
        }
    }
}
