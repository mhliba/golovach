package lesson01;
/*
implement in-place method, that "mixes" the values in array (!)randomly
print initial array "before" and "after"
*/

import java.util.Arrays;

public class Task07 {
    public static void main(String[] args) {
        int[] array = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        System.out.println("before:\t" + Arrays.toString(array));
        shakeArray(array);
        System.out.println("after:\t" + Arrays.toString(array));
    }

    private static void shakeArray(int[] array) {
        for (int i = 0; i < array.length ; i++) {
            swapValuesByIndices(array, i, (int) (Math.random() * array.length - 1));
        }
    }

    private static void swapValuesByIndices(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}