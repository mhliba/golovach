package lesson01;
/*
create an array with 11 elements;
initialize array;
write method for recursive in-place inversion of array:
show that it really works
*/

import java.util.Arrays;

public class Task05 {
    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,0};
        System.out.println(Arrays.toString(array));
        invertInplaceRecursive(array, 0);
        System.out.println(Arrays.toString(array));
    }

    private static void invertInplaceRecursive(int[] array, int valToMirrorSwap) {
        if (array.length<valToMirrorSwap+1) {return;}
            swapValueByIndices(array, valToMirrorSwap, array.length - 1 - valToMirrorSwap++);
            if (valToMirrorSwap < array.length / 2) {invertInplaceRecursive(array,valToMirrorSwap);}
        }

    private static void swapValueByIndices(int[] array, int firstIndex, int secondIndex) {
        int temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}