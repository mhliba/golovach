package lesson01;
/*
print half-rectangle using nested loops like following:
++++++++++
-+++++++++
--++++++++
---+++++++
----++++++
-----+++++
------++++
-------+++
--------++
---------+
 */
public class Task03 {
    public static void main(String[] args) {
        for (int col=0;col<10;col++){
            for (int row = 0; row < 10; row++) {
                if (col>row) {System.out.print("-");}
                else{System.out.print("+");}
            }
            System.out.println();
        }
    }
}
