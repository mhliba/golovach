package lesson02;
/*
implement method ,that performs bubble sort (in-place)
+visual demonstration of correctness of the algorithm
+performance test and comparison with Arrays.sort()
 */

import java.util.Arrays;

public class Task05 {
    private static final int MAX_RANDOM_NUM = 11;
    private static final int ELEMENTS_COUNT_PERF_TEST = 30_000;
    private static final int ELEMENTS_COUNT_CORR_TEST = 10;

    public static void main(String[] args) throws InterruptedException {
        Integer[] arrayCorrectness = new Integer[ELEMENTS_COUNT_CORR_TEST];
        reinitializeRandom(arrayCorrectness);

        System.out.println("=======CORRECTNESS=TEST========");
        System.out.println("Arrays.toString(array)  before : \n" + Arrays.toString(arrayCorrectness));
        System.out.println("-------------------------------");
        bubbleSort(arrayCorrectness);
        System.out.println("Arrays.toString(array)  after  : \n" + Arrays.toString(arrayCorrectness));

        Integer[] arrayPerformance = new Integer[ELEMENTS_COUNT_PERF_TEST];
        long start;
        long timeElapsed;

        reinitializeRandom(arrayPerformance);
        System.out.println("=======PERFORMANCE=TEST========");
        System.out.println("bubbleSort start!");
        start = System.currentTimeMillis();
        bubbleSort(arrayPerformance);
        timeElapsed = System.currentTimeMillis() - start;
        System.out.format("time elapsed : %.3fs\n", timeElapsed / 1000.);

        reinitializeRandom(arrayPerformance);
        System.out.println("-------------------------------");
        System.out.println("Arrays.sort start!");
        start = System.currentTimeMillis();
        Arrays.sort(arrayPerformance);
        timeElapsed = System.currentTimeMillis() - start;
        System.out.format("time elapsed : %.3fs", timeElapsed / 1000.);
    }

    private static <T extends Comparable<? super T>> void bubbleSort(T[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j].compareTo(array[j + 1]) > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    private static void reinitializeRandom(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * MAX_RANDOM_NUM);
        }
    }

    private static void reinitializeReverted(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length - i;
        }
    }
}
