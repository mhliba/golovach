package lesson02;
/*
implement method, that returns index of element with specified value in the sorted array
and returns -(expected_location_index)-1 if there is no such element in array
static int binarySearch(int[] array, int element){
*/

import java.util.Arrays;

public class Task02 {
    public static void main(String[] args) {
        int[] array = new int[]{0,1,2,3,4,5,6,7,8,9,9,9,9,9,9,10,11,12,13,14,15,16,16,16,16,16};
        System.out.println("binarySearch(array,6) = " + binarySearch(array, 9));
        System.out.println("Arrays.binarySearch(array,17) = " + Arrays.binarySearch(array, 9));
    }

    static int binarySearch(int[] array, int key) {
        if (array.length==0) {return -1;}
        int low = 0;
        int high = array.length;

        while (high>low) {
            int mid=(low+high)>>>1;
            int midvalue=array[mid];

            System.out.println("midIndex = " + mid);
            System.out.println("high = " + high);
            System.out.println("low = " + low);
            System.out.println("=====================");

            if (midvalue > key) {
                high=mid-1;
            }
            else if (midvalue < key) {
                low=mid+1;
            }
            else if (midvalue==key) {
                return mid;
            }
        }
        return ~low;
    }
}
