package lesson02;
/*
implement method, that returns index of element in the array with specified value
and returns -1 if there is no such element in array
int linearSearch(int[] array, int element)
 */

public class Task01 {
    public static void main(String[] args) {
        int[] array = new int[]{1,2,3,4,5,6,7,8,9,0,666};
        System.out.println("linearSearch(array,6) = " + linearSearch(array, 6));
        System.out.println("linearSearch(array,999996) = " + linearSearch(array, 999996));
    }

    static int linearSearch(int[] array, int element){
        for (int i = 0; i < array.length; i++) {
            if (array[i]==element) {return i;}
        }
        return -1;
    }
}
