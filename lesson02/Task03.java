package lesson02;
/*
a)implement method, that merges two array into the big one and returns it.
b)implement method, that merges two array into the big one using System.arrayCopy and returns it.
static int[] mergeArrays(int[] array1, int[] array2)
static int[] mergeArraysFast(int[] array1, int[] array2)
 */

import java.util.Arrays;

public class Task03 {
    public static void main(String[] args) {
        int[] firstArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
        int[] secondArray = new int[]{-1, -2, -3, -4, -5, -6, -7, -8, -9, -10, -11, -12, -13, -14, -15};
        int[] merged = mergeArrays(firstArray, secondArray);
        int[] mergedFast = mergeArraysFast(firstArray, secondArray);
        System.out.println("Arrays.toString(merged)     = " + Arrays.toString(merged));
        System.out.println("Arrays.toString(mergedFast) = " + Arrays.toString(mergedFast));
    }

    static int[] mergeArrays(int[] array1, int[] array2) {
        int[] result = new int[array1.length + array2.length];
        int destPos = 0;
        for (int num : array1) {
            result[destPos++] = num;
        }
        for (int num : array2) {
            result[destPos++] = num;
        }
        return result;
    }

    static int[] mergeArraysFast(int[] array1, int[] array2) {
        int[] result = new int[array1.length + array2.length];
        System.arraycopy(array1, 0, result, 0, array1.length);
        System.arraycopy(array2, 0, result, array1.length, array2.length);
        return result;
    }
}
