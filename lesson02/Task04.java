package lesson02;
/*
a)implement method, that merges two sorted array into the big sorted one and returns it.
program should have correct output with differently sorted (ascending,descending order) arrays.
output must be sorted in ascending order
method should be out-place.
public static <T extends Comparable<? super T>> T[] mergeSortedArrays(T[] first, T[] second)

 */

import java.lang.reflect.Array;
import java.util.Arrays;

public class Task04 {
    public static void main(String[] args) {
        String[] firstArray = new String[]{"a", "bb", "ccc", "cccccc", "hhhhh", "ppp", "qqqqq", "zzzzz"};
        String[] secondArray = new String[]{"a", "a", "ddd", "eeeee", "ff", "iiii", "jjjjj", "xxx"};
        String[] merged = mergeSortedArrays(firstArray, secondArray);

        System.out.println("Arrays.toString(firstArray)  before = " + Arrays.toString(firstArray));
        System.out.println("Arrays.toString(secondArray) before = " + Arrays.toString(secondArray));
        System.out.println("Arrays.toString(merged)             = " + Arrays.toString(merged));
        System.out.println("Arrays.toString(firstArray)  after  = " + Arrays.toString(firstArray));
        System.out.println("Arrays.toString(secondArray) after  = " + Arrays.toString(secondArray));
    }

    public static <T extends Comparable<? super T>> T[] mergeSortedArrays(T[] first, T[] second) {
        //handling 0-length args
        if (first.length == 0 && second.length == 0) {
            return second;
        } else if (first.length == 0) {
            return second;
        } else if (second.length == 0) {
            return first;
        }

        //check if one of arrays (or both) has descending-order sorting
        if (first[first.length - 1].compareTo(first[0]) < 0) {
            first = getReversed(first);
        }
        if (second[second.length - 1].compareTo(second[0]) < 0) {
            second = getReversed(second);
        }

        int newLength = first.length + second.length;
        T[] result = getNewArrayInstance(first, newLength);

        //merging itself
        int index1 = 0;
        int index2 = 0;
        for (int i = 0; i < result.length; i++) {
            if (index1 < first.length && index2 < second.length) {
                if (first[index1].compareTo(second[index2]) < 0) {
                    result[i] = first[index1++];
                } else {
                    result[i] = second[index2++];
                }
            } else if (index1 < first.length) {
                result[i] = first[index1++];
            } else if (index2 < second.length) {
                result[i] = second[index2++];
            }
        }
        return result;
    }

    private static <T extends Comparable<? super T>> T[] getNewArrayInstance(T[] typeExample, int length) {
        return (T[]) Array.newInstance(typeExample.getClass().getComponentType(), length);
    }

    private static <T extends Comparable<? super T>> T[] getReversed(T[] array) {
        T[] resultArray = getNewArrayInstance(array, array.length);
        int destination = array.length - 1;
        for (T element : array) {
            resultArray[destination--] = element;
        }
        return resultArray;
    }

}



