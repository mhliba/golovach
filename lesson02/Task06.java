package lesson02;
/*
implement method ,that performs selection sort (in-place)
+visual demonstration of correctness of the algorithm
+performance test and comparison with Arrays.sort()
 */

import java.util.Arrays;

public class Task06 {
    private static final int MAX_RANDOM_NUM = 11;
    private static final int ELEMENTS_COUNT_PERF_TEST = 30_000;
    private static final int ELEMENTS_COUNT_CORR_TEST = 10;

    public static void main(String[] args) throws InterruptedException {
        Integer[] arrayCorrectness = new Integer[ELEMENTS_COUNT_CORR_TEST];
        reinitializeRandom(arrayCorrectness);

        System.out.println("=======CORRECTNESS=TEST========");
        System.out.println("Arrays.toString(array)  before : \n" + Arrays.toString(arrayCorrectness));
        System.out.println("-------------------------------");
        selectionSort(arrayCorrectness);
        System.out.println("Arrays.toString(array)  after  : \n" + Arrays.toString(arrayCorrectness));

        Integer[] arrayPerformance = new Integer[ELEMENTS_COUNT_PERF_TEST];
        long start;
        long timeElapsed;

        reinitializeRandom(arrayPerformance);
        System.out.println("=======PERFORMANCE=TEST========");
        System.out.println("selectionSort start!");
        start = System.currentTimeMillis();
        selectionSort(arrayPerformance);
        timeElapsed = System.currentTimeMillis() - start;
        System.out.format("time elapsed : %.3fs\n", timeElapsed / 1000.);

        reinitializeRandom(arrayPerformance);
        System.out.println("-------------------------------");
        System.out.println("Arrays.sort start!");
        start = System.currentTimeMillis();
        Arrays.sort(arrayPerformance);
        timeElapsed = System.currentTimeMillis() - start;
        System.out.format("time elapsed : %.3fs", timeElapsed / 1000.);
    }

    private static <T extends Comparable<? super T>> void selectionSort(T[] array) {
        int minIndex;
        T tmp;
        for (int i = 0; i < array.length-1; i++) {
            minIndex=i;
            for (int j = i+1; j < array.length; j++) {
                if (array[j].compareTo(array[minIndex])<0) {
                    minIndex=j;
                }
            }
            if (minIndex!=i) {
                tmp = array[minIndex];
                array[minIndex] = array[i];
                array[i] = tmp;
            }

        }
    }

    private static void reinitializeRandom(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * MAX_RANDOM_NUM);
        }
    }

    private static void reinitializeReverted(Integer[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = array.length - i;
        }
    }
}
