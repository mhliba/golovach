package lesson02;
/*
implement method ,that performs creating and multiplying
of matrices with random content,or throws
IllegalArgumentException if it's not possible
with specified args

show the result to user.
 */

public class Task08 {
    private static final int MAX_RANDOM_NUM = 11;

    public static void main(String[] args) throws InterruptedException {
        int[][] matrixA = getRandomMatrix(2, 3);//new int[][]{{4,2,8}};
        int[][] matrixB = getRandomMatrix(3, 2);//new int[][]{{4},{3},{2}};
        printMatrix(matrixA);
        printMatrix(matrixB);
        int[][] result = getProduct(matrixA, matrixB);
        printMatrix(result);
    }

    private static int[][] getProduct(int[][] matrix1, int[][] matrix2) {
        boolean argsAreNotValid = !AreEligibleToMultiply(matrix1, matrix2);
        if (argsAreNotValid) { throw new IllegalArgumentException("You just can't do that. Deal with it"); }
        int[][] result = new int[matrix1.length][matrix2[0].length];
        System.out.println("\n"+matrix1.length+"x"+matrix2[0].length);

        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                for (int k = 0; k <matrix1[0].length; k++) {
                    result[i][j]+=matrix1[i][k]*matrix2[k][j];
                }
            }
        }
        return result;
    }

    private static boolean AreEligibleToMultiply(int[][] matrix1, int[][] matrix2) {
        if (nonRectangularOrEmpty(matrix1) || nonRectangularOrEmpty(matrix2)) {return false;  }
        return matrix1[0].length == matrix2.length;
    }

    //returns true if matrix is non-rectangular and has count of elements >0
    private static boolean nonRectangularOrEmpty(int[][] matrix) {
        if (matrix.length == 0) {
            return true;
        }
        int firstRowLength = matrix[0].length;
        for (int[] row : matrix) {
            if (row.length != firstRowLength) {
                return true;
            }
        }
        return false;
    }

    private static void printMatrix(int[][] matrix1) {
        for (int[] row : matrix1) {
            System.out.print("[\t");
            for (int num : row) {
                System.out.print(num + "\t");
            }
            System.out.print("]\n");
        }
        System.out.println();
    }

    private static int[][] getRandomMatrix(int i, int j) {
        int[][] matrix = new int[i][j];
        for (int k = 0; k < matrix.length; k++) {
            matrix[k] = getRandomArray(j);
        }
        return matrix;
    }

    private static int[] getRandomArray(int length) {
        int[] result = new int[length];
        for (int i = 0; i < result.length; i++) {
            result[i] = (int) (Math.random() * MAX_RANDOM_NUM);
        }
        return result;
    }
}